package app;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Tag;

import java.math.BigInteger;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Security unit tests")
@Tag("security")
public class appSecuritySpec {

    @Test
    public void BiggerThanIntMaxSizeNeedsApproval() {
        Main app = new Main();
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            boolean res = app.approval(new Amount(2147483647+1));
        });
    }

    @Test
    public void LessThanIntMinSizeNeedsApproval() {
        Main app = new Main();
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            boolean res = app.approval(new Amount(-2147483647-1));
        });
    }
}
