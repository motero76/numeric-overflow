package app;

public class Amount {
    private Integer amount;

    public Amount(Integer amount) {
        checkAmount(amount);
        this.amount = amount.intValue();
    }

    private void checkAmount(Integer amount) {
        if(amount >= Integer.MAX_VALUE) {
            throw new IllegalArgumentException("amount exceeds max value");
        }
        if (amount <= Integer.MIN_VALUE) {
            throw new IllegalArgumentException("amount less than min value");
        }
    }

    public Integer getAmount() {
        return amount;
    }
}
